<?php session_start();
unset($_SESSION["uid"]);
unset($_SESSION["uname"]);
unset($_SESSION["email"]);
unset($_SESSION["mobile"]);
session_destroy();
header("location:index.php");
?>