<?php session_start();
require('requires/db_config.php');
require('requires/head_link.php');
require('requires/lib/component.php');
error_reporting(E_ERROR | E_PARSE);
if(isset($_POST["btnSignIn"]))
{
    global $db;
    $email=htmlspecialchars(trim($_POST["InputEmail"]));
    $pass=htmlspecialchars(trim($_POST["InputPassword"]));
    $login_sql="SELECT id,id_card_no, name, email, mobile, department_id, designation_id, religion_id, gender_id,marital_status,shop_name, joining_date,present_are,present_thana_id,present_district_id,present_division_id,permanent_area,permanent_thana_id, permanent_districts_id, permanent_division_id, status_active, is_delete, create_by, create_at, update_by, update_at FROM tbl_users WHERE  email='".$email."' and password='".$pass."'";
    $result=$db->query($login_sql);

    if($db->affected_rows>0)
    {
        list($id,$id_card_no, $name, $email, $mobile, $department_id, $designation_id, $religion_id, $gender_id,$marital_status,$shop_name, $joining_date,$present_are,$present_thana_id,$present_district_id,$present_division_id,$permanent_area,$permanent_thana_id, $permanent_districts_id, $permanent_division_id, $status_active, $is_delete, $create_by, $create_at, $update_by, $update_at)=$result->fetch_row();

        $_SESSION["uid"]=$id;
        $_SESSION["uid_card_no"]=$id_card_no;
        $_SESSION["uname"]=$name;
        $_SESSION["email"]=$email;
        $_SESSION["mobile"]=$mobile;
        $_SESSION["create_by"]=$create_by;
        $_SESSION["create_at"]=$create_at;
        $_SESSION["update_by"]=$update_by;
        $_SESSION["update_at"]=$update_at;
        $_SESSION["gender_id"]=$gender_id;
        $_SESSION["is_delete"]=$is_delete;

        $_SESSION["shop_name"]=$shop_name;
        $_SESSION["present_are"]=$present_are;
        $_SESSION["religion_id"]=$religion_id;
        $_SESSION["joining_date"]=$joining_date;
        $_SESSION["department_id"]=$department_id;
        $_SESSION["status_active"]=$status_active;
        $_SESSION["designation_id"]=$designation_id;
        $_SESSION["permanent_area"]=$permanent_area;
        $_SESSION["marital_status"]=$marital_status;
        $_SESSION["present_thana_id"]=$present_thana_id;
        $_SESSION["permanent_thana_id"]=$permanent_thana_id;
        $_SESSION["present_division_id"]=$present_division_id;
        $_SESSION["present_district_id"]=$present_district_id;
        $_SESSION["permanent_division_id"]=$permanent_division_id;
        $_SESSION["permanent_districts_id"]=$permanent_districts_id;
        header("location:dashboard");
    }else
    {
        //$msg=danger_alert(["title"=>"User name and Password is incorrect!"]);
        $msg="Not Working";

    }
}//end isset
?>
<body onload=" welCome()">
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-center auth px-0">
                <div class="row w-100 mx-0">
                    <div class="col-lg-4 mx-auto">
                        <div class="auth-form-light text-left py-5 px-4 px-sm-5">
                            <div class="brand-logo">
                                <img src="assets/images/logo.svg" alt="logo">
                                <?php if(isset($_POST["btnSignIn"]))
                                {
                                    ?>
                                    <h3 class="alert alert-danger "><?php echo $msg;?></h3>
                                    <?php
                                }
                                ?>
                            </div>
                            <form class="pt-3"  method="POST" autocomplete="off">
                                <div class="form-group">
                                    <input type="email" class="form-control form-control-lg" name="InputEmail" id="InputEmail" placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control form-control-lg" id="InputPassword" placeholder="Password" name="InputPassword">
                                </div>
                                <div class="mt-3 text-xxl-end">
                                    <button type="submit" class="btn btn-block btn-primary btn-sm" name="btnSignIn" id="btnSignIn">SIGN IN</button>
                                </div>
                                <div class="my-2 d-flex justify-content-between align-items-center">
                                    <a href="#" class="auth-link text-black">Forgot password?</a>
                                </div>
                                <div class="text-center mt-4 fw-light">
                                    Don't have an account? <a href="register.php" class="text-primary">Create</a>
                                </div>
                            </form>
                        </div>
                    </div>
                 </div>
            </div>
        <!-- content-wrapper ends -->
        </div>
    <!-- page-body-wrapper ends -->
    </div>
<?php require('requires/script.php');?>
</body>

</html>
