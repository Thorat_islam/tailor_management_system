<?php session_start();
require('requires/db_config.php');
require('requires/head_link.php');
require('requires/lib/component.php');
require('requires/lib/array_function.php');
if (!isset($_SESSION["uid"])) { header("location:index.php");}
?>
<body onload="welCome()">
  <div class="container-scroller"> 
    <!-- partial:partials/_navbar.html -->
      <?php include('requires/navbar.php');?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <?php include('requires/left_sidebar.php');?>
      <!-- partial -->
      <div class="main-panel" style="margin-left: 220px;">
        <div class="content-wrapper p-2">
            <?php include('router/web.php');?>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
          <footer class="footer" style="position:;">
              <div class="d-sm-flex justify-content-center justify-content-sm-between ">
                  <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Developed By <a href="" target="">Md Thorat Islam</a></span>
                  <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Copyright © 2022</span>
              </div>
          </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <?php require('requires/script.php');?>
<script>
    $(document).ready(function ($)
    {
        $(".nav-item .nav-link").click(function (event)
        {
            $(".nav-item .nav-link").addClass("active");
        });
    });
</script>
</body>
</html>

