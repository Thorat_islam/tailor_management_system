DROP SCHEMA IF EXISTS `db_tailor_shop`;
CREATE SCHEMA `db_tailor_shop` DEFAULT CHARACTER SET utf8mb4 ;
DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE `tbl_users` (
                             `id` int(11) NOT NULL,
                             `name` varchar(50) DEFAULT NULL,
                             `email` varchar(100) DEFAULT NULL,
                             `mobile` int(11) DEFAULT NULL,
                             `department_id` int(11) DEFAULT NULL,
                             `designation_id` int(11) DEFAULT NULL,
                             `religion_id` int(11) DEFAULT NULL,
                             `gender_id` int(11) DEFAULT NULL,
                             `marital_status` int(11) DEFAULT NULL,
                             `company_id` int(11) DEFAULT NULL,
                             `joining_date` date DEFAULT NULL,
                             `present_are` varchar(150) DEFAULT NULL,
                             `present_thana_id` int(11) DEFAULT NULL,
                             `present_district_id` int(11) DEFAULT NULL,
                             `present_division_id` int(11) DEFAULT NULL,
                             `permanent_area` varchar(150) DEFAULT NULL,
                             `permanent_thana_id` int(11) DEFAULT NULL,
                             `permanent_districts_id` int(11) DEFAULT NULL,
                             `permanent_division_id` int(11) DEFAULT NULL,
                             `password` VARCHAR(150) NOT NULL,
                             `status_active` int(11) DEFAULT NULL,
                             `is_delete` int(11) DEFAULT NULL,
                             `create_by` int(11) DEFAULT NULL,
                             `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                             `update_by` int(11) DEFAULT NULL,
                             `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `tbl_users`
    ADD PRIMARY KEY (`id`);
ALTER TABLE `tbl_users`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

INSERT INTO `tbl_users` (`id`, `id_card_no`, `name`, `email`, `mobile`, `department_id`, `designation_id`,
                         `religion_id`, `gender_id`, `marital_status`, `company_id`, `joining_date`, `present_are`,
                         `present_thana_id`, `present_district_id`, `present_division_id`, `permanent_area`,
                         `permanent_thana_id`, `permanent_districts_id`, `permanent_division_id`, `status_active`,
                         `password`, `is_delete`, `create_by`, `create_at`, `update_by`, `update_at`)
VALUES (NULL, ''TS-23-0001'', ''Md Thorat Islam'', ''test@mail.com'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''1 '', ''123 '', ''0 '', NULL, CURRENT_TIMESTAMP, NULL, NULL);