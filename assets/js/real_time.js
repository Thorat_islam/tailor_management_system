$(document).ready(function() {
    clockUpdate();
    setInterval(clockUpdate, 1000);
})

function clockUpdate() {
    var date = new Date();
    // $('.digital-clock').css({'color': 'black', 'text-shadow': '0 0 6px black'});
    // $('.digital-clock').css({'color': 'black', 'text-shadow': '0 0 3px #FF0000'});
    $('.digital-clock').css({'color': '#BDB76B', 'text-shadow': '1px 1px 0px black, 0 0 0px blue', 'box-shadow':'2px 5px 10px 2px lightblue','background':'redlinear-gradient(90deg, #000, #555)'});
    function addZero(x) {
        if (x < 10) {
            return x = '0' + x;
        } else {
            return x;
        }
    }

    function twelveHour(x) {
        if (x > 12) {
            return x = x - 12;
        } else if (x == 0) {
            return x = 12;
        } else {
            return x;
        }
    }


    var h = addZero(twelveHour(date.getHours()));
    var m = addZero(date.getMinutes());
    //var s = addZero(date.getSeconds());
    // var ampm = h <= 12 ? 'pm' : 'am';
    var ampm = addZero(date.getHours()) >= 12 ? 'pm' : 'am';

    $('.digital-clock').text(h + ':' + m + ' ' + ampm)
}