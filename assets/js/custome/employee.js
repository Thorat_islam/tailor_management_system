// =============Employee ID Card Number Ganarate==============================
    $(document).ready(function () {
        $.ajax({
            url: 'pages/employees/requires/id_number_ganarate.php',
            type: 'post',
            data: {},
            success: function (result) {
                $("#id_card_number").val(result);
            }
        });
    });
// ===========================================================================


// =================Employee Add Form Image Privew============================
    function emp_photo(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img_upload_tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#img_upload").change(function () {
        emp_photo(this);
    });
// ============================================================================


// ============Nid Image Privew ===============================================
    function emp_nid_fst(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#nid_fst_upload_tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#nid_fst_upload").change(function () {
        emp_nid_fst(this);
    });
    function emp_nid_scnd(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#nid_scnd_upload_tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#nid_scnd_upload").change(function () {
        emp_nid_scnd(this);
    });
// ============================================================================


// =================Employee Add Form Image Privew=============================
    function emp_photo(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img_upload_tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#img_upload").change(function () {
        emp_photo(this);
    });
// ============================================================================


// ==================Nid Image Privew =========================================
    function emp_nid_fst(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#nid_fst_upload_tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#nid_fst_upload").change(function () {
        emp_nid_fst(this);
    });
    function emp_nid_scnd(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#nid_scnd_upload_tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#nid_scnd_upload").change(function () {
        emp_nid_scnd(this);
    });
// ============================================================================


//========= permanent Address Hide=============================================
    $(document).ready(function() {
        $('#chk[type="checkbox"]').click(function()
        {
            $("#permanent_show").toggle();
        });
    });
//=============================================================================


//====================Insert Oparetion ========================================
    $('#btnSubmit').on('click',function (event)
    {
        let fullName                = $('#txtFullName').val();
        let id_card                 = $('#id_card_number').val();
        let mobile                  = $('#txtMobile').val();
        let gender                  = $("input[name='chckGender']:checked").val();
        let dob                     = $('#txtDob').val();
        let degisnation             = $('#cbo_degisnation').val();
        let department              = $('#cbo_department').val();
        let riligion                = $('#cbo_riligion').val();
        let email                   = $('#txtEmail').val();
        let marital_status          = $('#cbo_marital_status').val();

        let shopName                = $('#cbo_shopName').val();
        let joiningDate             = $('#txtJoiningDate').val();
        let password                = $('#txtPassword').val();
        let nidNumber               = $('#txtNidNumber').val();

        let division_present        = $('#cbo_division_prent').val();
        let district_present        = $('#cbo_districts_prent').val();
        let thana_present           = $('#cbo_thana_prent').val();
        let area_present            = $('#textAreaPrent').val();

        let division_perm           = $('#cbo_division_perm').val();
        let district_perm           = $('#cbo_districts_perm').val();
        let thana_perm              = $('#cbo_thana_perm').val();
        let area_perm               = $('#textAreaperm').val();

          alert(joiningDate);
        if ($("input[name='sameAddressChkBox']").is(':checked'))
        {
            division_perm           =   division_present;
            district_perm           =   district_present;
            thana_perm              =   thana_present;
            area_perm               =   area_present;
        }
        //alert(division_perm+"**"+district_perm+"**"+thana_perm);
        //alert(division_present + "**" + district_present + "**" + thana_present);
        event.preventDefault();
        let valid = false;
        $("input[type='file']").each(function()
        {
            if ($(this).val() != '')
            {
                valid = true;
            }
        });
        if (!valid)
        {
            show_msg_alert('Please Upload your Photo',1);
            $('#photo_required_msg').text('Please Upload your Photo').addClass('text-danger');
        }
        if (valid)
        {
            if (!fullName)
            {
                show_msg_alert('Please Entry Your Name',1);
                $('#name_required_msg').text('Please Entry Your Name').addClass('text-dark bg-inverse-danger');
            }
            else if(!mobile){
                show_msg_alert('Please Entry Your Mobile Number',1);
                $('#mobile_required_msg').text('Please Entry Your Mobile Number').addClass('text-dark bg-inverse-danger');
            }
            else if($("input[name='chckGender']:checked").length == 0){
                show_msg_alert('Please Select your gender',1);
                $('#gender_required_msg').text('Please Select your gender').addClass('text-dark bg-inverse-danger');
            }
            else if(!dob)
            {
                show_msg_alert('Enter Your Date of Birth',1);
                $('#dob_required_msg').text('Enter Your Date of Birth').addClass('text-dark bg-inverse-danger');
            }
            else if(degisnation == null){
                show_msg_alert('Select Your Degisnation',1);
                $('#degisnation_required_msg').text('Select Your Degisnation').addClass('text-dark bg-inverse-danger');
            }
            else if(riligion == null){
                show_msg_alert('Select Your Religion',1);
                $('#reiligion_required_msg').text('Select Your Religion').addClass('text-dark bg-inverse-danger');
            }
            else if (!email){
                show_msg_alert('Please Entry Your Email',1);
                $('#email_required_msg').text('Please Entry Your Email').addClass('text-dark bg-inverse-danger');
            }
            else if(marital_status == null){
                show_msg_alert('Select Your Marital Status',1);
                $('#marital_required_msg').text('Select Your Marital Status').addClass('text-dark bg-inverse-danger');
            }
            else if($("input[name='txtJoiningDate']").val() == "")
            {
                show_msg_alert('Enter Your Joning Date',1);
                $('#dob_required_msg').text('Enter Your Joning Date').addClass('text-dark bg-inverse-danger');
            }
            else if(!password){
                show_msg_alert('Please Entry Your Password',1);
                $('#pass_required_msg').text('Please Entry Your Password').addClass('text-dark bg-inverse-danger');
            }
            else if(!nidNumber){
                show_msg_alert('Entry Your Nid Number',1);
                $('#nid_required_msg').text('Entry Your Nid Number').addClass('text-dark bg-inverse-danger');
            }
            else{

                // let formData = new FormData($("#form_employee")[0]);
                let formData        = new FormData();
                let photo           = $('#img_upload')[0].files[0];
                let nidF            = $('#nid_fst_upload')[0].files[0];
                let nidL            = $('#nid_scnd_upload')[0].files[0];
                //Form all input field data store
                let all_data        = (id_card+"**"+fullName+"**"+mobile+"**"+gender+"**"+dob+"**"+degisnation+"**"+riligion+"**"+email+"**"+marital_status+"**"+shopName+"**"+joiningDate+"**"+password+"**"+nidNumber+"**"+division_present+"**"+district_present+"**"+thana_present+"**"+area_present+"**"+division_perm+"**"+district_perm+"**"+thana_perm+"**"+area_perm+"**"+department);

                formData.append('photo', photo);
                formData.append('nidF', nidF);
                formData.append('nidL', nidL);

                formData.append('all_data',all_data);
                //alert(all_data);

                $.ajax
                ({
                    url: "pages/employees/requires/insert.php",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data)
                    {

                        if (!data)
                        { //If fails
                            if (data.errors.name)
                            {
                                $('#emp_success_msg').fadeIn(1000).html('Test'); //Throw relevant error
                            }
                        }
                        else if (data > 0)
                        {
                            var dialog = bootbox.dialog({
                                //message: '<p class="text-center text-success bg-inverse-secondary">Data Save Succcessfully...</p>',
                                message: '<p class="text-center mb-0 text-success bg-inverse-secondary">Data Save Succcessfully</p>',
                                closeButton: false
                            }).find('.modal-content').css({'background-color': '#21e095', 'font-weight' : 'bold', color: 'white', 'font-size': '1em', 'font-weight' : 'bold'} );
                            setTimeout(function()
                            {
                                dialog.modal('hide');
                                //$('#form_employee').trigger('submit');
                            },2000);
                        }else if (data > 0){
                            if (data.errors.name)
                            {
                                $('#emp_success_msg').fadeIn(1000).html('Test'); //Throw relevant error
                            }
                        }
                    }
                });
            }
        }
    });
//=============================================================================

//==================Employee View Button click action==========================
$('body').on('click','#btn_view',function (){
    let emp_id=$(this).data('emp_id');
    //alert(emp_id);
    $.ajax({
        url         :       'pages/employees/requires/profile_info_data.php',
        
    });
});
//=============================================================================
//==================Profile Information Load===================================
    function profile_info(){
        $.ajax({
            url     :       '',
            type    :       '',
            data    :       '',
            success :       function (info_data){
                $('#infoData').html(info_data);
            }
        });

    }
