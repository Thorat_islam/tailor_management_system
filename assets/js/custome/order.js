$('#txt-search').keyup(function()
{
    $('.next').prop('disabled', true);
    var searchField = $(this).val();
    //alert(searchField);
    var regex = new RegExp(searchField, "i");
    var output = '';
    
    if(searchField == '')
    {
      $('#filter-records').html('');
      return;
    }
      $.ajax
      ({
          url       :       'pages/order/requires/customer_search.php',
          type      :       'POST',
          dataType  :       'JSON',
          data      :       {'searchField':searchField},
          success   :       function (data)
          {
            $.each(data, function(key, val)
            {
              //alert(val);
                var fullname = val.name ;
                //alert(fullname+'=');
                // if ((fullname.search(regex) != -1))
                // {
                 // alert(fullname+'=');
                    output += '<li id="' +val.id +'" class="li-search" data-name="'+val.name+'" data-mobile="'+val.mobile+'" data-email="'+val.email+'" >'+ val.name+' - ('+val.mobile+')</li>';
                // }
            });
            $('#filter-records').html(output);

            $(document).on("click", ".li-search", function ()
            {
              let name      =   $(this).data("name");
              let mobile    =   $(this).data("mobile");
              let email    =   $(this).data("email");


              $("#txt-search").val(name);
              $("#filter-records").html("");

              if ($(".li-search").attr("id") != false) 
              {
                $('#txtFullName').val(name);
                $('#txtMobile').val(mobile);
                $('#txtEmail').val(email);
              } else
              {
                // EMPTY USER SEARCH INPUT
                $("#txt-search").val('');
              }
              $(".next").prop("disabled", false);
            });

          }

      });
});



$(".radio-group .radio").on("click", function () 
{
  $(".selected .fa").removeClass("fa-check");
  $(".radio").removeClass("selected");
  $(this).addClass("selected");
  if ($("#suser").hasClass("selected") == true) {
    $(".next").prop("disabled", true);
    $(".searchfield").show();
  } else {
    setFormFields(false);
    $(".next").prop("disabled", false);
    $("#filter-records").html("");
    $(".searchfield").hide();
  }
});
var step = 1;
$(document).ready(function () { stepProgress(step); });

$(".next").on("click", function () {
  var nextstep = false;
  if (step == 2) {
    nextstep = checkForm("userinfo");
  } else {
    nextstep = true;
  }
  if (nextstep == true) {
    if (step < $(".step").length) {
      $(".step").show();
      $(".step")
        .not(":eq(" + step++ + ")")
        .hide();
      stepProgress(step);
    }
    hideButtons(step);
  }
});

// ON CLICK BACK BUTTON
$(".back").on("click", function () {
  if (step > 1) {
    step = step - 2;
    $(".next").trigger("click");
  }
  hideButtons(step);
});

// CALCULATE PROGRESS BAR
stepProgress = function (currstep) {
  var percent = parseFloat(100 / $(".step").length) * currstep;
  percent = percent.toFixed();
  $(".progress-bar")
    .css("width", percent + "%")
    .html(percent + "%");
};

// DISPLAY AND HIDE "NEXT", "BACK" AND "SUMBIT" BUTTONS
hideButtons = function (step)
{
  var limit = parseInt($(".step").length);
  $(".action").hide();
  if (step < limit) {
    $(".next").show();
  }
  if (step > 1) {
    $(".back").show();
  }
  if (step == limit) {
    $(".next").hide();
    $(".submit").show();
  }
};
function setFormFields(id) 
{
 
  if (id != false) {
    // FILL STEP 2 FORM FIELDS
    d = data.find(x => x.id === id);
    $('#txtFullName').val(d.name);
  } else {
    // EMPTY USER SEARCH INPUT
    $("#txt-search").val('');
    $('#name').val('');
  }
}

function checkForm(val) {
  // CHECK IF ALL "REQUIRED" FIELD ALL FILLED IN
  var valid = true;
  $("#" + val + " input:required").each(function () {
    if ($(this).val() === "") {
      $(this).addClass("is-invalid");
      valid = false;
    } else {
      $(this).removeClass("is-invalid");
    }
  });
  return valid;
}
