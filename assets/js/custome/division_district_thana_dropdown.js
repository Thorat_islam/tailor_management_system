// ===================Present District Select division wise ===================
$('#cbo_division_prent').on('change',function (){
    //alert('Test');
    let division_id = $('#cbo_division_prent').val();
    let type=0;
    //alert(division_id);
    $.ajax
    ({
        url         :   'pages/employees/requires/districts_dropdown.php',
        type        :   'post',
        cache       :   false,
        data        :   {'division_prent' : division_id, 'type' : type},
        success     :   function (r)
        {
            $('#cbo_districts_prent').children().remove().end().html(r);
        }
    });
});
// ============================================================================


//====================Present Thana Select District wise=======================
$('#cbo_districts_prent').on('change',function ()
{
    //alert('Test');
    let division_id = $('#cbo_division_prent').val();
    let districts_prent = $('#cbo_districts_prent').val();
    let type=0;
    //alert(division_id+" "+districts_prent);
    $.ajax
    ({
        url         :   'pages/employees/requires/upazila_dropdown.php',
        type        :   'post',
        cache       :   false,
        data        :   {'division_prent' : division_id, 'districts_prent' : districts_prent, 'type' : type},
        success     :   function (r)
        {
            $('#cbo_thana_prent').children().remove().end().html(r);
        }
    });
});
//=============================================================================


// ===================Permanent District Select division wise =================
$('#cbo_division_perm').on('change',function (){
    //alert('Test');
    let division_id_parm = $('#cbo_division_perm').val();
    let type=1;
    // alert(division_id);
    $.ajax
    ({
        url         :   'pages/employees/requires/districts_dropdown.php',
        type        :   'post',
        cache       :   false,
        data        :   {'division_parm' : division_id_parm, 'type' : type},
        success     :   function (r)
        {
            $('#cbo_districts_perm').children().remove().end().html(r);
        }
    });
});
// ============================================================================


//====================Permanent Thana Select District wise=====================
$('#cbo_districts_perm').on('change',function ()
{
    //alert('Test');
    let division_id = $('#cbo_division_perm').val();
    let districts_perm = $('#cbo_districts_perm').val();
    let type=1;
    //alert(division_id+" "+districts_prent);
    $.ajax
    ({
        url         :   'pages/employees/requires/upazila_dropdown.php',
        type        :   'post',
        cache       :   false,
        data        :   {'division_perm' : division_id, 'districts_perm' : districts_perm, 'type' : type},
        success     :   function (r)
        {
            $('#cbo_thana_perm').children().remove().end().html(r);
        }
    });
});
//=============================================================================
