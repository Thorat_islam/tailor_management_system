<?php
if (isset($_GET['page']))
{
    $page=$_GET['page'];
    $not_found=1;

   if ($page== "dashboard")
   {
       include('pages/dashboard/view.php');
       $not_found=0;
   }elseif ($page== "profile")
   {
       include('pages/profile/view.php');
       $not_found=0;
   }elseif ($page=="employees_list"){
       include('pages/employees/employees_list.php');
       $not_found=0;
   }elseif ($page=="employees_add"){
       include('pages/employees/employee_add.php');
       $not_found=0;
   }elseif ($page=="create_order"){
    include('pages/order/create_order.php');
    $not_found=0;
    }

    if ($not_found)
    {
        //include('pages/error/404.php');
        echo "No Page Found";
    }
}else
{
    include('pages/dashboard/view.php');
}
?>