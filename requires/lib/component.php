<?php

    function nav_link($url,$text,$icon="")
    {
        ?>
        <a class="nav-link" href="<?php echo $url;?>">&nbsp;<i class="<?php echo $icon;?>"></i>
            <span class="menu-title"><?php echo $text;?></span>
        </a>
        <?php
    }
    function nav_link_dropdown($url,$text,$icon="",$options=array())
    {
        if(count($options))
        {
            ?>
            <a class="nav-link" data-bs-toggle="collapse" href="#ui-<?php echo strtolower(trim($text))?>" aria-expanded="false" aria-controls="ui-<?php echo strtolower(trim($text))?>">
                <i class="<?php echo $icon?>"></i>
                <span class="menu-title"><?php echo $text?></span>
                <i class="menu-arrow"></i>
            </a>
            <?php
        }else{
            nav_link($url,$text,$icon);
        }
        if(count($options))
        {
            ?>
            <div class="collapse" id="ui-<?php echo strtolower(trim($text))?>">
                <?php
                 nav_dropdown($options);
                ?>
            </div><?php
        }
    }
    function nav_dropdown($options)
    {
        ?>
            <ul class="nav flex-column sub-menu">
                <?php foreach ($options as $option)
                { ?>
                    <li class="nav-item">
                        <?php echo nav_link($option["url"],$option["text"]);?>
                    </li>
                    <?php
                } ?>
            </ul>
        <?php
    }

    function dashboard_card($query,$title)
    {
        global $db;
        $nameArray = $db->query($query);
        list($data_fld_name)=$nameArray->fetch_row();
        ?>
        <div class="col-sm-2 p-0 shadow-lg me-2 mt-2">
            <div class="statistics-details d-flex align-items-center justify-content-between ">
                <div class="d-none d-md-block">
                    <p class="statistics-title"><?php echo $title;?></p>
                    <h3 class="rate-percentage"><?php echo $data_fld_name;?></h3>
                    <p class="text-success d-flex">
                        <i class="mdi mdi-menu-down"></i><span><?php echo $data_fld_name/2;?>%</span>
                    </p>
                </div>
            </div>
        </div>
        <?php
    }
    function return_library_array($query, $id_fld_name, $data_fld_name)
    {
        global $db;
        $nameArray = $db->query($query);
        foreach ($nameArray as $result) {
            $new_array[$result[$id_fld_name]] = $result[$data_fld_name];
        }
        return $new_array;
    }
    function cbo_dropdown($query,$id_fld_name, $data_fld_name,$id,$name,$select_massage,$type,$select,$disable)
    {
       if ($disable != 0){
           ?>
           <select disabled class="form-select form-select-sm" id="<?php echo $id ?>" name="<?php echo $name ?>" required>
           <?php
       }else{
           ?>
           <select class="form-select form-select-sm" id="<?php echo $id ?>" name="<?php echo $name ?>" required>
           <?php
       }
        if ($select != 1)
        {
        ?>
            <option selected disabled value="0">----<?php echo $select_massage ?>----</option>
        <?php
        }

        if ($type == 0)
        {
            global $db;
            $nameArray = $db->query($query);
            foreach ($nameArray as $result)
            {
                if ($select == 1)
                {
                    //        $new_array[$result[$id_fld_name]] = $result[$data_fld_name];
                  ?>
                    <option selected value="<?php echo $result[$id_fld_name];?>"><?php echo $result[$data_fld_name]?></option>
                <?php
                }else{
                    ?>
                    <option value="<?php echo $result[$id_fld_name];?>"><?php echo $result[$data_fld_name]?></option>
                    <?php
                }
            }

        }
        else
        {
            $nameArray = $query;
            foreach ($nameArray as $key => $result)
            {
    //        $new_array[$result[$id_fld_name]] = $result[$data_fld_name];
                ?>
                <option value="<?php echo $key;?>"><?php echo $result?></option>
                <?php
            }
        }
        ?>
        </select>
        <?php
    }
    function input_filed($config, $css=[])
    {

        $config["id"]=isset($config["id"])?$config["id"]:"";
        $config["type"]=isset($config["type"])?$config["type"]:"";
        $config["name"]=isset($config["name"])?$config["name"]:"";
        $config["value"]=isset($config["value"])?$config["value"]:"";
        $config["label"]=isset($config["label"])?$config["label"]:"";
        $config["required"]=isset($config["required"])?$config["required"]:"";
        $config["readonly"]=isset($config["readonly"])?$config["readonly"]:"";
        $config["disabled"]=isset($config["disabled"])?$config["disabled"]:"";
        $config["placeholder"]=isset($config["placeholder"])?$config["placeholder"]:"";
        $config["required_msg"]=isset($config["required_msg"])?$config["required_msg"]:"";
        ?>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="<?php echo $config["id"]?>" >
                    <?php echo $config["label"];?>
                </label>
                <div class="col-sm-9 text-center">
                    <input type="<?php echo $config["type"] ?>" name="<?php echo $config["name"]?>" id="<?php echo $config["id"]?>" class="form-control" placeholder="<?php echo  $config["placeholder"]?>" value="<?php echo $config["value"]?>" <?php echo $config["required"]?> <?php echo $config["readonly"]?> <?php echo $config["disabled"];?>
                    style="<?php
                        if(count($css)>1)
                        {
                            //echo "<pre>";
                            //print_r($css);
                           foreach ($css as $val){
                                echo $val['property'].":".$val['value'].";";
                           }
                        }
                    ?>"
                    >
                    <span id="<?php echo $config["required_msg"]?>" style="font-size: 12px"></span>
                </div>
            </div>
        <?php
    }
?>