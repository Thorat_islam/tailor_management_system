
<!--=============jquery-3.6.3.js ====================-->
<script src="assets/js/jquery-3.6.3.js"></script>
<!-- plugins:js -->
<script src="assets/vendors/js/vendor.bundle.base.js" xmlns=""></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<!--<script src="assets/vendors/chart.js/Chart.min.js"></script>-->
<script src="assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!--<script src="assets/vendors/progressbar.js/progressbar.min.js"></script>-->

<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="assets/js/off-canvas.js"></script>
<script src="assets/js/hoverable-collapse.js"></script>
<script src="assets/js/template.js"></script>
<script src="assets/js/settings.js"></script>
<script src="assets/js/todolist.js"></script>
<!-- endinject -->

<!-- Custom js for this page-->
<script src="assets/js/dashboard.js"></script>
<script src="assets/js/real_time.js"></script>
<!--<script src="assets/js/Chart.roundedBarCharts.js"></script>-->
<!--<script src="assets/js/dataTables.select.min.js"></script>-->
<!--<script src="assets/js/data-table.js"></script>-->
<script src="assets/js/custome/employee.js"></script>
<script src="assets/js/custome/division_district_thana_dropdown.js"></script>
<!-- End custom js for this page-->

<script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.3.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.print.min.js"></script>
<!--Bootbox plugin=============-->
<script src=" https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js "></script>

<script src="assets/js/custome/order.js"></script>
<script>
    function welCome() {
        var dd = new Date();
        var hh = dd.getHours();
        //alert(dd);

        if (hh >= 6 && hh <= 11)
        {
            $(".welcome-text").text('Good Morning, ').append('<span class="text-black fw-bold"><?php echo $_SESSION['uname'];?></span>')
        }
        else if (hh >= 12 && hh <= 15)
        {
            $(".welcome-text").text('Good Noon, ').append('<span class="text-black fw-bold"><?php echo substr($_SESSION['uname'],0,9);?></span>')
        }
        else if (hh >= 16 && hh <= 18)
        {
            $(".welcome-text").text('Good Afternoon, ').append('<span class="text-black fw-bold"><?php echo $_SESSION['uname'] ?></span>')

        }
        else if (hh >= 19 && hh <= 24)
        {
            $(".welcome-text").text('Good Night, ').append('<span class="text-black fw-bold"><?php echo $_SESSION['uname'] ?></span>')

        }else {
            $(".welcome-text").text('.............');
        }

    }
    $(document).ready(function() {
        $('#example').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        } );
    } );
    function show_msg_alert(msg,type)
    {
        if (type != 1)
        {
            // let bg_color = 'background-color': '#12b877';
            let emapty_alert = bootbox.alert({message: msg,className: 'rubberBand animated'}).find('.modal-content').css({'padding':'5px','background-color': '#12b877', 'font-weight' : 'bold', color: 'white', 'font-size': '1em', 'font-weight' : 'bold'} );
            setTimeout(function()
            {
                emapty_alert.modal('hide');
            },5000);
        }else {
            // let bg_color = 'background-color': '#cf1b09';
            let emapty_alert = bootbox.alert({message: msg,className: 'rubberBand animated'}).find('.modal-content').css({'padding':'5px','background-color': '#cf1b09', 'font-weight' : 'bold', color: 'white', 'font-size': '1em', 'font-weight' : 'bold'} );
            setTimeout(function()
            {
                emapty_alert.modal('hide');
            },5000);
        }
    }
    var triggerTabList = [].slice.call(document.querySelectorAll('#myTab a'))
    triggerTabList.forEach(function (triggerEl) {
        var tabTrigger = new bootstrap.Tab(triggerEl)

        triggerEl.addEventListener('click', function (event) {
            event.preventDefault()
            tabTrigger.show()
        })
    });



    //==================================================================================================================



</script>