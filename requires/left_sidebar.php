<nav class="sidebar sidebar-offcanvas shadow p-0 mb-0 bg-white rounded" id="sidebar" style="top: 100px;position: fixed;">
    <ul class="nav">
      <?php
      if ($_SESSION["designation_id"] == 1)
      {
          ?>
          <li class="nav-item">
              <?php
              nav_link_dropdown("dashboard","Dashboard","mdi mdi-speedometer ");
              ?>
          </li>

          <li class="nav-item nav-category">Order</li>
          <li class="nav-item">
              <?php
              $order_menu=[
                  ["url"=>"create_order","text"=>"Create Order"],
                  ["url"=>"#","text"=>"Order List"],
              ];
              echo nav_link_dropdown("#","Order","mdi mdi-account-multiple ",$order_menu);
              ?>
          </li>
          <li class="nav-item nav-category">Customer</li>
          <!--          <i class=""></i>-->
          <li class="nav-item">
              <?php
              $customer_menu=[
                  ["url"=>"#","text"=>"Create Customer"],
                  ["url"=>"#","text"=>"Customer List"],
              ];
              echo nav_link_dropdown("#","Customer","mdi mdi-account-star-outline",$customer_menu);
              ?>
          </li>
          <li class="nav-item nav-category">Employees</li>
          <li class="nav-item">
              <?php
              $options=[
                  ["url"=>"employees_list","text"=>"Employees List"],
                  ["url"=>"employees_add","text"=>"Employee Add"],
              ];
              echo nav_link_dropdown("#","Employees","mdi mdi-account-multiple ",$options);
              ?>
          </li>
          <?php
      }
      ?>
    </ul>
</nav>
