
<nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex align-items-top flex-row">
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-start">
        <div class="me-3">
            <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-bs-toggle="minimize">
                <span class="icon-menu"></span>
            </button>
        </div>
        <div>
            <a class="navbar-brand brand-logo" href="dashboard">
                <img src="assets/images/big_logo.jpg" alt="logo" style="width: 120px; height: 70px"/>
            </a>
            <a class="navbar-brand brand-logo-mini" href="dashboard">
                <img src="assets/images/logo-mini.svg" alt="logo" />
            </a>
        </div>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-top">
        <ul class="navbar-nav">
            <li class="nav-item font-weight-semibold d-none d-lg-block ms-0">
                <h1 class="welcome-text">Good Morning,</h1>
                <h3 class="welcome-sub-text digital-clock"></h3>
            </li>
        </ul>
        <ul class="navbar-nav ms-auto">
            <li class="nav-item d-none d-lg-block">
                <div id="datepicker-popup" class="input-group date datepicker navbar-date-picker">
                  <span class="input-group-addon input-group-prepend border-right">
                    <span class="icon-calendar input-group-text calendar-icon"></span>
                  </span>
                    <input type="text" class="form-control" readonly>
                </div>
            </li>

            <li class="nav-item dropdown d-none d-lg-block user-dropdown">
                <a class="nav-link" id="UserDropdown" href="#" data-bs-toggle="dropdown" aria-expanded="false">
                    <?php
                        if (file_exists("assets/images/employees/profile/".strtolower($_SESSION['uid_card_no']).".jpg"))
                        {
                            ?>
                            <img class="img-xs rounded-circle" src="assets/images/employees/profile/<?php echo strtolower($_SESSION['uid_card_no'])?>.jpg" alt="Profile image">
                            <?php
                        }elseif (file_exists("assets/images/employees/profile/".strtolower($_SESSION['uid_card_no']).".png"))
                        {
                            ?>
                            <img class="img-xs rounded-circle" src="assets/images/employees/profile/<?php echo strtolower($_SESSION['uid_card_no'])?>.png" alt="Profile image">
                            <?php
                        }
                       /// echo $row['name'];
                    ?>
                   </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                    <div class="dropdown-header text-center">

<!--                        <img class="img-md rounded-circle" src="assets/images/faces/face8.jpg" alt="Profile image">-->
                        <?php
                        if (file_exists("assets/images/employees/profile/".strtolower($_SESSION['uid_card_no']).".jpg"))
                        {
                            ?>
                            <img width="80" class="img-md rounded-circle" src="assets/images/employees/profile/<?php echo strtolower($_SESSION['uid_card_no'])?>.jpg" alt="Profile image">
                            <?php
                        }elseif (file_exists("assets/images/employees/profile/".strtolower($_SESSION['uid_card_no']).".png"))
                        {
                            ?>
                            <img width="80" class="img-md rounded-circle" src="assets/images/employees/profile/<?php echo strtolower($_SESSION['uid_card_no'])?>.png" alt="Profile image">
                            <?php
                        }
                        /// echo $row['name'];
                        ?>
                        <p class="mb-1 mt-3 font-weight-semibold"><?php echo $_SESSION['uname']?></p>
                        <p class="fw-light text-muted mb-0"><?php echo $_SESSION['email']?></p>
                    </div>
                    <a class="dropdown-item" href="profile&user_id=<?php echo $_SESSION['uid']?>">
                        <i class="dropdown-item-icon mdi mdi-account-outline text-primary me-2"></i>
                        My Profile
<!--                        <span class="badge badge-pill badge-danger">1</span>-->
                    </a>



                    <a class="dropdown-item" href="logout.php"><i class="dropdown-item-icon mdi mdi-power text-primary me-2"></i>Sign Out</a>
                </div>
            </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-bs-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
        </button>
    </div>
</nav>
