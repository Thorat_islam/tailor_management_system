<?php session_start();
require('Employee.Class.php');
date_default_timezone_set("Asia/Dhaka");
$timestamp = time();
$pc_date_time = date('Y-m-d h:i:s', $timestamp);
//$pc_date_time       =   date("Y-m-d h:i:sa");
$user_id            =   $_SESSION['uid'];
$all_data           =   htmlspecialchars($_POST["all_data"]);

$data               =   explode('**',$all_data);
$id_card            =   $data[0];
$fullName           =   htmlspecialchars($data[1]);
$mobile             =   $data[2];
$gender             =   $data[3];
$dob                =   $data[4];
$degisnation        =   $data[5];
$riligion           =   $data[6];
$email              =   htmlspecialchars($data[7]);
$marital_status     =   $data[8];
$shopName           =   $data[9];
$joiningDate        =   date_format(date_create($data[10]),"Y-m-d");
$password           =   htmlspecialchars($data[11]);
$nidNumber          =   $data[12];

$division_present   =   $data[13];
$district_present   =   $data[14];
$thana_present      =   $data[15];
$area_present       =   htmlspecialchars($data[16]);

$division_perm      =   $data[17];
$district_perm      =   $data[18];
$thana_perm         =   $data[19];
$area_perm          =   htmlspecialchars($data[20]);
$department          =  $data[21];

$employee           =   new Employee('',$id_card,$fullName,$email,$mobile,$department,$degisnation,$riligion,$gender,$marital_status,$shopName,$joiningDate,$area_present,$thana_present,$district_present,$division_present,$area_perm,$thana_perm,$district_perm,$division_perm,$nidNumber,1,$password,0,'null',$user_id ,$pc_date_time,'','');

if ($_FILES)
{
    $id             =   $employee->insert($_FILES);
    if ($id > 0)
    {
        echo 1; // insert success message
    }
    elseif ($id ==  -1)
    {
        echo -1;    // Photo type ivalite formate
    }
    elseif ($id ==  -2)
    {
        echo -2;    // Photo Size problem
    }
    elseif ($id ==  -3)
    {
        echo -3;    // NID part 1 type ivalite formate
    }elseif ($id ==  -4)
    {
        echo -4;    // NID part 1 type Size problem
    }
    elseif ($id ==  -5)
    {
        echo -5;    // NID part 2 type ivalite formate
    }elseif ($id ==  -6)
    {
        echo -6;    // NID part 2 type Size problem
    }
}
