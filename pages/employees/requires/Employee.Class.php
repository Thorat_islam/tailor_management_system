<?php require('../../../requires/db_config.php');
class Employee
{
    public $id;
    public  $id_card_no;
    public $name;
    public $email;
    public $mobile;
    public $department_id;
    public $designation_id;
    public $religion_id;
    public $gender_id;
    public $marital_status;
    public $shop_name;
    public $joining_date;
    public $present_are;
    public $present_thana_id;
    public $present_district_id;
    public $present_division_id;
    public $permanent_area;
    public $permanent_thana_id;
    public $permanent_districts_id;
    public $permanent_division_id;
    public $nid;
    public $status_active;
    public $password;
    public $is_delete;
    public $leave_date;
    public $create_by;
    public $create_at;
    public $update_by;
    public $update_at;
    function __construct($id,$id_card_no,$name,$email,$mobile,$department_id,$designation_id,$religion_id,$gender_id,$marital_status,$shop_name,$joining_date,$present_are,$present_thana_id,$present_district_id,$present_division_id,$permanent_area,$permanent_thana_id,$permanent_districts_id,$permanent_division_id,$nid,$status_active,$password,$is_delete,$leave_date,$create_by,$create_at,$update_by,$update_at)
    {
        $this->id=null?"":$id;                                  $this->id_card_no=$id_card_no;
        $this->name=$name;                                      $this->email=$email;
        $this->mobile=$mobile;                                  $this->department_id=$department_id;
        $this->designation_id=$designation_id;                  $this->religion_id=$religion_id;
        $this->gender_id=$gender_id;                            $this->marital_status=$marital_status;
        $this->shop_name=$shop_name;                            $this->joining_date=$joining_date;
        $this->nid=$nid;                                        $this->password=$password;
        $this->leave_date=$leave_date;

        $this->present_are=$present_are;                        $this->present_thana_id=$present_thana_id;
        $this->present_district_id=$present_district_id;        $this->present_division_id=$present_division_id;

        $this->permanent_area=$permanent_area;                  $this->permanent_thana_id=$permanent_thana_id;
        $this->permanent_districts_id=$permanent_districts_id;  $this->permanent_division_id=$permanent_division_id;

        $this->status_active=$status_active;                    $this->is_delete=$is_delete;
        $this->create_by=$create_by;                            $this->create_at=$create_at;
        $this->update_by=$update_by;                            $this->update_at=$update_at;
    }

    function insert($FILES="")
    {
        global $db;
        $insert_sql="INSERT INTO tbl_users(id_card_no,name,email, mobile, department_id, designation_id, religion_id, gender_id, marital_status, shop_name, joining_date, present_are, present_thana_id, present_district_id, present_division_id, permanent_area, permanent_thana_id, permanent_districts_id, permanent_division_id, nid, status_active, password, is_delete, create_by, create_at) 
        VALUES ('$this->id_card_no','$this->name','$this->email','$this->mobile','$this->department_id','$this->designation_id','$this->religion_id','$this->gender_id','$this->marital_status','$this->shop_name','$this->joining_date','$this->present_are','$this->present_thana_id','$this->present_district_id','$this->present_division_id','$this->permanent_area','$this->permanent_thana_id','$this->permanent_districts_id','$this->permanent_division_id','$this->nid','$this->status_active','$this->password','$this->is_delete','$this->create_by','$this->create_at')";
         //print_r($photo);die();

        if (is_array($FILES))
        {

            $photo=$FILES['photo']['name'];
            $nid_part1=$FILES['nidF']['name'];
            $nid_part2=$FILES['nidL']['name'];
            //print_r($FILES['photo']);die();

            if (is_array($FILES['photo']))
            {
                //echo $FILES['photo']['name'];die();
                //print_r($FILES['photo']);die();
                $ext=pathinfo($FILES['photo']['name'],PATHINFO_EXTENSION);
                $size=$FILES['photo']["size"]/1024;
                $type=$FILES['photo']["type"];
                if ($type=="image/png" || $type=="image/jpeg" || $type=="image/jpg")
                {
                    if ($size<=1000)
                    {
                        $db->query($insert_sql);
                        move_uploaded_file($FILES['photo']["tmp_name"],"../../../assets/images/employees/profile/".strtolower($this->id_card_no).".{$ext}");
                        if (is_array($FILES['nidF']))
                        {
                            $ext=pathinfo($FILES['nidF']["name"],PATHINFO_EXTENSION);
                            $size=$FILES['nidF']["size"]/1024;
                            $type=$FILES['nidF']["type"];
                            if ($type=="image/png" || $type=="image/jpeg") {
                                if ($size<=1000) {
                                    move_uploaded_file($FILES['nidF']["tmp_name"],"../../../assets/images/employees/nid/".strtolower($this->id_card_no)."_part_a".".{$ext}");
                                } else {
                                    return -4;
                                }
                            } else {
                                return -3;
                            }
                        }
                        if (is_array($FILES['nidL']))
                        {
                            $ext=pathinfo($FILES['nidL']["name"],PATHINFO_EXTENSION);
                            $size=$FILES['nidL']["size"]/1024;
                            $type=$FILES['nidL']["type"];
                            if ($type=="image/png" || $type=="image/jpeg") {
                                if ($size<=1000) {
                                    move_uploaded_file($FILES['nidL']["tmp_name"],"../../../assets/images/employees/nid/".strtolower($this->id_card_no)."_part_b".".{$ext}");
                                } else {
                                    return -6;
                                }
                            } else {
                                return -5;
                            }
                        }

                    }
                    else
                    {
                        return -2;
                    }
                }
                else
                {
                    return -1;
                }
            }
        }
        return $db->insert_id;
    }
    function update()
    {
        global $db;
        $sql="UPDATE tbl_users SET name='$this->name',email='$this->email',mobile='$this->mobile',department_id='$this->department_id',designation_id='$this->designation_id',religion_id='$this->religion_id',`dob`='[value-9]',gender_id='$this->gender_id',marital_status='$this->marital_status',shop_name='$this->shop_name',joining_date='$this->joining_date',present_are='$this->present_are',present_thana_id='$this->present_thana_id',present_district_id='$this->present_district_id',present_division_id='$this->present_division_id',permanent_area='$this->permanent_area',permanent_thana_id='$this->permanent_thana_id',permanent_districts_id='$this->permanent_districts_id',permanent_division_id='$this->permanent_division_id',nid='$this->nid',status_active='$this->status_active',password='$this->password',is_delete='$this->is_delete',leave_date='$this->leave_date',update_by='$this->update_by',update_at='$this->update_at' WHERE id = '$this->id'";
        $db->query($sql);
    }
    function delete(){}
    function profile_info()
    {
        global $db;

    }
}