<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Employees List</h4>
                <div class="table-responsive">
                    <table  id="example" class="table table-striped">
                        <thead>
                            <tr>
                                <th style="width: 40px">Sl</th>
                                <th style="width: 120px">ID Card</th>
                                <th style="width: 150px">First name</th>
                                <th style="width: 100px">Mobile</th>
                                <th style="width: 100px">Designation</th>
                                <th style="width: 100px">Religion</th>
                                <th style="width: 100px">Joining Date</th>
                                <th style="width: 60px;">Day of Service</th>
                                <th style="width: 100px">Status</th>
                                <th style="width: 100px">Action</th>
                            </tr>
                        </thead>
                        <tbody id="records_table">
                        <?php
                        global $db;
                        $shop_arr=return_library_array('SELECT id,name from tbl_shop_name where status_active=1 and is_delete=0','id','name');
                        $designation_arr=return_library_array('SELECT id,name from  tbl_designation where status_active=1 and is_delete=0','id','name');

                        $sql_emp="SELECT id,id_card_no,name, email,mobile,department_id,designation_id,religion_id,gender_id,marital_status,shop_name,joining_date,present_are,present_thana_id,present_district_id,present_division_id,permanent_area,permanent_thana_id,permanent_districts_id,permanent_division_id, status_active,password,is_delete,leave_date,create_by,create_at,update_by, update_at FROM tbl_users";
                        $result=$db->query($sql_emp);
                        $i=1;
                        foreach ($result as $row)
                        {
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td><?php echo $row['id_card_no'];?></td>
                                <td class="py-2">
                                    <?php
                                        if (file_exists("assets/images/employees/profile/".$row['id_card_no'].".jpg")) {
                                            ?>
                                            <img src="assets/images/employees/profile/<?php echo $row['id_card_no']?>.jpg">
                                            <?php
                                        }
                                        elseif (file_exists("assets/images/employees/profile/".$row['id_card_no'].".jpeg")) {
                                            ?>
                                            <img src="assets/images/employees/profile/<?php echo $row['id_card_no']?>.jpeg">
                                            <?php
                                        }
                                        elseif (file_exists("assets/images/employees/profile/".$row['id_card_no'].".png"))
                                        {
                                            ?>
                                            <img src="assets/images/employees/profile/<?php echo $row['id_card_no']?>.png">
                                            <?php
                                        }
                                    ?>
                                    <a href="profile&user_id=<?php echo $row['id']?>">
                                    <?php echo $row['name'];
                                    ?>
                                    </a>
                                </td>
                                <td><?php echo $row['mobile'];?></td>
                                <td><?php echo $designation_arr[$row['designation_id']];?></td>
                                <td><?php echo $religion_arr[$row['religion_id']];?></td>
                                <td><?php echo $row['joining_date'];?></td>
                                <td>
                                    <?php
                                    $bday = new DateTime($row['joining_date']); // Your date of birth
                                    if (!empty($row['leave_date'])){
                                        $today = new Datetime($row['leave_date']);
                                    }else{
                                        $today = new Datetime(date('y-m-d'));
                                    }
                                    $diff = $today->diff($bday);
                                    echo "Days: ". $diff->d."<br>Months :".$diff->m."<br>Year :".$diff->y;
                                    ?>
                                </td>
                                <td><?php
                                    if ($row['status_active'] !=1)
                                    {
                                        ?>
                                        <span class="text-danger"><?php echo  $status_arr[$row['status_active']];?></span>
                                        <?php
                                    }else
                                    {
                                        ?>
                                        <span class="text-success"><?php echo  $status_arr[$row['status_active']];?></span>
                                        <?php

                                    }


                                    ?>
                                </td>
                                <td>
                                    <a href=""  class="btn btn-inverse-success btn-icons" id="btn_view" data-emp_id="<?php echo $row['id']?>">
                                        <i class="ti-eye"></i>
                                    </a>
<!--                                    <button type="button" id="btn_view" class="btn btn-inverse-success btn-icons" data-emp_id="--><?php ////echo $row['id']?><!--"><i class="ti-eye"></i></button>-->
                                    <button class="btn btn-inverse-danger btn-icons">
                                        <i class="ti-trash"></i>
                                    </button>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Button trigger modal -->
<!-- Modal -->
<!--<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">-->
<!--    <div class="modal-dialog">-->
<!--        <div class="modal-content">-->
<!--            <div class="modal-header">-->
<!--                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>-->
<!--                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>-->
<!--            </div>-->
<!--            <div class="modal-body">-->
<!--                ...-->
<!--            </div>-->
<!--            <div class="modal-footer">-->
<!--                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>-->
<!--                <button type="button" class="btn btn-primary">Save changes</button>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
