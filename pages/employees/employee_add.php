<?php
$company_arr=return_library_array('SELECT id,name from tbl_shop_name where status_active=1 and is_delete=0','id','name');

?>
<style>
    input[type='number'] {
        -moz-appearance:textfield;
    }

    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
    }
    .alert-custom{
        z-index: 1;
        opacity: 1;
        width: 53%;
        left: 465px;
        top: 178px;
        display: none;
    }
</style>
<div class="row position-relative">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title position-absolute mt-5">Employee Add</h4>
                <div class="text-end p-0 m-0 px-0 py-0 position-relative">
                    <label for="img_upload">
                        <img src="assets/images/employees/profile/add_employee_thumbnail.png" id="img_upload_tag" style="width: 160px;height: 160px;border: 1px solid black" class="img-thumbnail"/>
                    </label>
                </div>
                <form class="form-sample" id="form_employee" method="post" enctype="multipart/form-data">
                    <!--==========Personal Information ================================-->
                    <div class="row">
                        <div class="col-md-6">
                            <?php
                                $card_no=["label" => "Card Number","type" => "text","name" => "id_card_number","id" => "id_card_number","required" => "required","readonly" => "readonly"];
                                $css=[
                                        ['property' => 'color','value'=>'#1041A2'],
                                        ['property' => 'font-size','value'=>'18px'],
                                        ['property' => 'font-weight','value'=>'bold']
                                    ];
                                echo  input_filed($card_no,$css);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                                $photo=["label" => "Photo","type" => "file","name" => "img_upload", "id" => "img_upload", "required" => "required","required_msg" => "photo_required","required_msg" => "photo_required_msg"];
                                echo  input_filed($photo);
                            ?>
<!--                            <span id="photo_required"></span>-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?php
                                $full_name=["label" => "Full Name *","type" => "text","name" => "txtFullName","id" => "txtFullName","required" => "required","placeholder" => "Full Name","required_msg" => "name_required_msg"];
                                echo  input_filed($full_name);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                                $mobile=["label" => "Mobile *","type" => "number","name" => "txtMobile","id" => "txtMobile","required" => "required","placeholder" => "+8801700000000","required_msg" => "mobile_required_msg"];
                                echo  input_filed($mobile);
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="chckGender1">Gender</label>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="chckGender" id="chckGender" value="1" required>
                                            Male
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="chckGender" id="chckGender" value="2" required>
                                            Female
                                        </label>
                                    </div>
                                    <span id="gender_required_msg" style="font-size: 12px"></span>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <?php
                                $dob=["label" => "Date of Birth *","type" => "date","name" => "txtDob","id" => "txtDob","required" => "required","required_msg" => "dob_required_msg"];
                                echo  input_filed($dob);
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="cbo_degisnation">Designation *</label>
                                <div class="col-sm-9">
                                    <?php
                                    $sql_desg="SELECT id,name from tbl_designation WHERE id !=1 and status_active=1 and is_delete=0";
                                    echo cbo_dropdown($sql_desg,'id','name','cbo_degisnation','cbo_degisnation','Select Degisnation',0,0,0);?>

                                    <span id="degisnation_required_msg" style="font-size: 12px"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="txtRiligion">Religion *</label>
                                <div class="col-sm-9">
                                    <?php
                                    echo cbo_dropdown($religion_arr,'','','cbo_riligion','cbo_riligion','Select Religion',1,0,0);?>
                                    <span id="reiligion_required_msg" style="font-size: 12px"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="cbo_department">Department *</label>
                                <div class="col-sm-9">
                                    <?php
                                    $sql_dep="SELECT id,name from tbl_department WHERE status_active=1 and is_delete=0";
                                    echo cbo_dropdown($sql_dep,'id','name','cbo_department','cbo_department','Select Department',0,1,1);?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <?php
                            $email=["label" => "Email *","type" => "email","name" => "txtEmail","id" => "txtEmail","required" => "required","placeholder" => "example@mail.com","required_msg" => "email_required_msg"];
                            echo  input_filed($email);
                            ?>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="cbo_marital_status">Marital Status *</label>
                                <div class="col-sm-9">
                                    <?php
                                    echo cbo_dropdown($marital_status_arr,'','','cbo_marital_status','cbo_marital_status','Select Marital Status',1,0,0);?>
                                    <span id="marital_required_msg" style="font-size: 12px"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="cbo_shopName">Shop Name *</label>
                                <div class="col-sm-9">
                                    <?php
                                    $sql_com="SELECT id,name from tbl_shop_name where status_active=1 and is_delete=0";
                                    echo cbo_dropdown($sql_com,'id','name','cbo_shopName','cbo_shopName','Select Shop Name',0,1,1);?>
                                </div>
                            </div>
                            <?php
//                            $shop=$company_arr[$_SESSION['shop_name']];
//                            $shop_name=["label" => "Shop Name *","type" => "text","name" => "txtShopName","id" => "txtShopName",
//                                "value" => $shop,
//                                "readonly" => "readonly"
//                            ];
//                            echo  input_filed($shop_name);
                            ?>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?php
                            $joining_date=["label" => "Joining Date *","type" => "date","name" => "txtJoiningDate","id" => "txtJoiningDate","required" => "required","required_msg" => "job_required_msg"];
                            echo  input_filed($joining_date);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label mt-5" for="">NID</label>
                                <div class="col-sm-5">
                                    <label for="nid_fst_upload">
                                        <img src="assets/images/employees/nid/thumbale_first_part.jpg" id="nid_fst_upload_tag" style="width: 220px;height: 160px;border: 1px solid black" class="img-thumbnail"/>
                                    </label>
                                    <input type="file" name="nid_fst_upload" id="nid_fst_upload" class="form-control" required>
                                </div>
                                <div class="col-sm-5">
                                    <label for="nid_scnd_upload">
                                        <img src="assets/images/employees/nid/thumbale_back_part.jpg" id="nid_scnd_upload_tag" style="width: 220px;height: 160px;border: 1px solid black" class="img-thumbnail"/>
                                    </label>
                                    <input type="file" name="nid_scnd_upload" id="nid_scnd_upload" class="form-control" required>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?php
                                $password=["label" => "Password *","type" => "password","name" => "txtPassword","id" => "txtPassword","required" => "required","placeholder" => "Create Password","required_msg" => "pass_required_msg"];
                                echo  input_filed($password);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                                $joining_date=["label" => "NID Number *","type" => "number","name" => "txtNidNumber","id" => "txtNidNumber","required" => "required","placeholder" => "NID Number Digit(10/13/17)","required_msg" => "nid_required_msg"];
                                echo  input_filed($joining_date);
                            ?>
                        </div>
                    </div>
                    <!--==========End==================================================-->

                    <!--===========Present Address=====================================-->
                    <p class="card-description">Present Address</p>
                    <hr class="cm-hr">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="cbo_division_prent">Division</label>
                                <div class="col-sm-9">
                                    <?php
                                    $sql_desg="SELECT id, division_name  from tbl_divisions WHERE status_active=1 and is_delete=0";
                                    echo cbo_dropdown($sql_desg,'id','division_name','cbo_division_prent','cbo_division_prent','----Select Degisnation----',0,0,0);?>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="cbo_districts_prent">Districts</label>
                                <div class="col-sm-9">
                                    <select class="form-select form-select-sm" name="cbo_districts_prent" id="cbo_districts_prent">
                                        <option value="0" selected disabled>Select Districts</option>
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="cbo_thana_prent">Thana</label>
                                <div class="col-sm-9">
                                    <select class="form-select form-select-sm" name="cbo_thana_prent" id="cbo_thana_prent">
                                        <option value="0" selected disabled>Select Districts</option>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label" for="txtArea">House No/ Area</label>
                                <div class="col-sm-9">
                                    <textarea name="textAreaPrent" id="textAreaPrent"  class="form-control"></textarea>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!--=======================End=======================================-->
                    <p class="card-description">Permanent Address</p>
                    <div id="per_show" class="row">
                        <label  class="col-sm-3 col-form-label">
                            <input type="checkbox" id="chk" name="sameAddressChkBox" value="1">
                            Same as Present Address
                        </label>
                    </div>
                    <div id="permanent_show">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label" for="cbo_division_perm">Division</label>
                                    <div class="col-sm-9">
                                        <?php
                                        $sql_desg="SELECT id, division_name  from tbl_divisions WHERE status_active=1 and is_delete=0";
                                        echo cbo_dropdown($sql_desg,'id','division_name','cbo_division_perm','cbo_division_perm','----Select Degisnation----',0,0,0);?>
                                        <!--                                    <textarea class="form-control" name="" id="" cols="30" rows="2"></textarea>-->
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label" for="cbo_districts_perm">Districts</label>
                                    <div class="col-sm-9">
                                        <select class="form-select form-select-sm" name="cbo_districts_perm" id="cbo_districts_perm">
                                            <option value="0" selected disabled>Select Districts</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label" for="cbo_thana_perm">Thana</label>
                                    <div class="col-sm-9">
                                        <select class="form-select form-select-sm" name="cbo_thana_perm" id="cbo_thana_perm">
                                            <option value="0" selected disabled>Select Districts</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label" for="txtArea">House No/ Area</label>
                                    <div class="col-sm-9">
                                        <textarea name="textAreaperm" id="textAreaperm"  class="form-control">

                                        </textarea>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="cm-hr">

                    <div class="row">
                        <div class="col-md-12 text-end">
                            <button type="submit" id="btnSubmit" class="btn btn-inverse-success">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
