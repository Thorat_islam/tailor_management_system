<?php
global $db;
$user_id=$_GET['user_id'];
$company_arr=return_library_array('SELECT id,name from tbl_shop_name where status_active=1 and is_delete=0','id','name');
$sql_emp="SELECT id,id_card_no,name, email,mobile,department_id,designation_id,religion_id,gender_id,marital_status,shop_name,joining_date,present_are,present_thana_id,present_district_id,present_division_id,permanent_area,permanent_thana_id,permanent_districts_id,permanent_division_id, status_active,password,is_delete,leave_date,create_by,create_at,update_by, update_at FROM tbl_users where id=$user_id";
$result=$db->query($sql_emp);
list($id,$id_card_no,$name)=$result->fetch_row();
?>
<div class="row">
    <div class="col-sm-4">
        <div class="card">
            <div class="card-header">
                <div class="card-title"></div>
            </div>
            <div class="card-body">
                <div class="card-img card-img-holder">
                    <?php
                    if (file_exists("assets/images/employees/profile/".$id_card_no.".jpg"))
                    {
                        ?>
                        <img  alt="" srcset="" class="w-50 rounded-circle mx-auto d-block" src="assets/images/employees/profile/<?php echo $id_card_no?>.jpg" alt="Profile image">
                        <?php
                    }elseif (file_exists("assets/images/employees/profile/".$id_card_no.".jpeg"))
                    {
                        ?>
                        <img  alt="" srcset="" class="w-50 rounded-circle mx-auto d-block" src="assets/images/employees/profile/<?php echo $id_card_no?>.jpeg" alt="Profile image">
                        <?php
                    }elseif (file_exists("assets/images/employees/profile/".$id_card_no.".png"))
                    {
                        ?>
                        <img src="assets/images/employees/profile/<?php echo $id_card_no?>.png">
                        <?php
                    }
                    ?>
                </div>
                <div class="card-text">
                   <p class="text-black font-monospace text-center" style="font-size: 18px"><?php echo $_SESSION['uname']?></p>
                   <p class="text-black font-monospace text-center" ><?php echo $_SESSION['email']?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-8 px-0 p-0">
        <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <button class="nav-link active" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Profile</button>
                        <button class="nav-link" id="nav-update-tab" data-bs-toggle="tab" data-bs-target="#nav-update" type="button" role="tab" aria-controls="nav-update" aria-selected="false">Update</button>
                        <button class="nav-link" id="nav-chngPass-tab" data-bs-toggle="tab" data-bs-target="#nav-chngPass" type="button" role="tab" aria-controls="nav-chngPass" aria-selected="false">Change Passwort</button>
                    </div>
                </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                <div class="row">
                    <div class="com-sm-12">
                        <table class="table table-bordered table-striped table-hover" style="width: 800px" id="infoData">
                            <caption class="caption-top text-center">Profile Information</caption>
                            <tr>
                                <th style="width: 80px"><b>Name</b></th>
                                <td style="width: 20px">:</td>
                                <td style="width: 690px"><?php echo $name;?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-update" role="tabpanel" aria-labelledby="nav-update-tab">...</div>
            <div class="tab-pane fade" id="nav-chngPass" role="tabpanel" aria-labelledby="nav-chngPass-tab">...</div>
        </div>

    </div>
</div>
