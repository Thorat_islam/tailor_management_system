<style>
    .li-search {
    list-style-type: none;
    padding: 0;
    margin: 0;
    border: 1px solid #ddd;
    margin-top: -1px;
    background-color: #f6f6f6;
    padding: 8px;
    font-size: 14px;
    color: black;
    display: block;
    cursor: pointer;
    }

    .li-search:hover {
    background-color: #eee;
    }

    .opt-icon {
    margin-top: 30px;
    margin-bottom: 20px
    }
    .card-block {
    width: 215px;
    border: 1px solid lightgrey;
    border-radius: 5px !important;
    background-color: #FAFAFA;
    margin-bottom: 30px
    }
    .radio {
    display: inline-block;
    border-radius: 0;
    box-sizing: border-box;
    cursor: pointer;
    color: #000;
    font-weight: 500;
    opacity: 0.6;
    }
    .radio.selected {
    box-shadow: 0px 8px 16px 0px #EEEEEE;
    opacity: 1;
    }
    .radio:hover {
    background-color: #d7f5e8;
    opacity: 1;
    }
    .selected {
    background-color: #e0f2ea;
    }

</style>
<div class="row">
    <div class="col-sm-12">
        <div class="container d-flex justify-content-center p-0" style="min-width:720px!important">
            <div class="col-11 col-offset-2">
                <div class="progress mt-3" style="height: 30px;">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" style="font-weight:bold; font-size:15px;" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    </div>
                </div>
                    <div class="card mt-3">
                    <div class="card-header font-weight-bold">Order Create
                    </div>
                        <div class="card-body p-4 step">
                            <div class="radio-group row justify-content-between px-3 text-center" style="justify-content:center !important">
                                <div class="col-auto me-sm-2 mx-1 card-block py-0 text-center radio">
                                    <div class="opt-icon">
                                        <i class="mdi mdi-account-plus" style="font-size: 80px; margin-left: 25px;"></i>
                                    </div>
                                    <p><b>Add new Customer</b></p>
                                </div>
                                <div id="suser" class="selected col-auto ms-sm-2 mx-1 card-block py-0 text-center radio">
                                    <div class="opt-icon">
                                        <i class="mdi mdi-account-search-outline" style="font-size: 80px;"></i>
                                    </div>
                                    <p><b>Search existing Customer</b></p>
                                </div>
                            </div>
                            <div class="searchfield input-group px-5">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="mdi mdi-account-search" aria-hidden="true"></i>
                            </span>
                                <input id="txt-search" class="form-control" type="text" placeholder="Search" aria-label="Search">
                            </div>
                            <div id="filter-records" class="mx-5"></div>
                        </div>
                        <div id="userinfo" class="card-body p-4 step" style="display: none">
                            <div class="text-center">
                                <h5 class="card-title font-weight-bold pb-2">Customer information</h5>
                            </div>
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="txtFullName">
                                        Full Name
                                        <b style="color: #dc3545;">*</b>
                                    </label>
                                    <input type="text" name="txtFullName" class="form-control" id="txtFullName" required>
                                    <div class="invalid-feedback">This field is required</div>
                                </div>
                                <div class="col-4">
                                    <label for="txtMobile">Mobile<b style="color: #dc3545;">*</b></label>
                                    <input type="text" class="form-control" id="txtMobile" name="txtMobile" required>
                                    <div class="invalid-feedback">This field is required</div>
                                </div>
                                <div class="col-4">
                                    <label for="txtEmail">Emil<b style="color: #dc3545;">*</b></label>
                                    <input type="text" class="form-control" id="txtEmail" name="txtEmail" required>
                                    <div class="invalid-feedback">This field is required</div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-4">
                                    <label for="lname">Gender<b style="color: #dc3545;">*</b></label>
                                    <!--                                <input type="text" class="form-control" id="lname" required>-->
                                    <?php echo cbo_dropdown($gender_arr,'','','cbo_marital_status','cbo_marital_status','Select Marital Status',1,0,0);?>
                                </div>

                            </div>
                        </div>
                        <div class="card-body p-5 step" style="display: none">Step 3</div>
                        <div class="card-body p-5 step" style="display: none">Step 4</div>
                        <div class="card-body p-5 step" style="display: none">Step 5</div>
                        <div class="card-footer">
                            <button class="action back btn btn-sm btn-outline-warning" style="display: none">Back</button>
                            <button class="action next btn btn-sm btn-outline-secondary float-end" disabled="">Next</button>
                            <button class="action submit btn btn-sm btn-outline-success float-end" style="display: none">Submit</button>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
